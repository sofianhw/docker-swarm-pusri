# Virtual Machine Era

Virtual machine (VM) is the virtualization/emulation of a computer system. Virtual machines are based on computer architectures and provide functionality of a physical computer. Their implementations may involve specialized hardware, software, or a combination.

## Needs of VM 
Pertumbuhan aplikasi yang sangat cepat
dimana dalam satu fisik server bisa jadi terdapat banyak aplikasi
implikasi nya apa?
walaupun bahasa yang sama bisa jadi 
* berbeda versi
* berbeda library dan versi
* kadang membutuhkan OS yang berbeda juga
* kebutuhan Resource yang berbeda
seperti (port, ram, cpu)

Sehingga hal ini akan sangat sulit untuk dimaintain dalam satu fisik server.
Namun jika menggunakan multiple server fisik, akan sangat mahal. Karena bisa jadi utilisasi dari aplikasi ini sangatlah minim.

Maka dari itu dibutuhkan virtualisasi mesin dalam satu fisik server.
sehingga masing masing aplikasi bisa jalan secara isolated virtualisasi yang berbeda di dalam satu mesin fisik server yang sama.

Ada 3 tipe Virtualisasi
* Bare-Metal
    * Hyper-V
* OS
    * VMWare
    * LXC

## VM are Expensive
- Need to predict the resource, every time changes require restart
- Need sometimes to provision and booting

# Welcoming Container
Lightweigth OS Virtulazation with shared kernel

gambar docker

## Container Orchestration
Platform that make easy to deploy, manage, and scale multi-container.
* Amazon EKS/ECS
* Apache Mesos
* Docker Swarm
* Kubernetes
* Google Kubernetes Engine
* Marathon
* etc

## What is Docker?

Docker is on open-platform for developers and sysadmins to build, ship, and run distributed applications

Consist of :
* Docker Engine, portable and lightweight runtime and packaging tool.
* Docker Hub, cloud service for sharing applications and automating workflows.

<iframe src="//www.slideshare.net/slideshow/embed_code/key/nnxV2wsevtc6Pr" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/sofianhw/docker-101-for-developer" title="Docker 101 for Developer" target="_blank">Docker 101 for Developer</a> </strong> from <strong><a href="https://www.slideshare.net/sofianhw" target="_blank">Sofian Hadiwijaya</a></strong> </div>

## Lets Install Docker

### Setup Repository

1. Uninstall jika sebelumnya pernah ada Docker

```bash=
sudo apt-get remove docker docker-engine docker.io containerd runc
```

2. Update dan Install minimum supporting packages
```shell=
 sudo apt-get update
 
 sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

3. Add Docker GPG Key
```shell=
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

4. Add stable repository
```shell=
echo \
"deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
### Install Docker Engine

1. Update Packages Index and Install Docker
```shell=
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
2. Verify Docker installed
```shell=
sudo docker run hello-world
```

## Post Install Docker

### Manage Docker as a non-root user

1. Create the docker group.
```shell=
sudo groupadd docker
```
2. Add your user to the docker group.
```shell=
sudo usermod -aG docker $USER
```
3. Log out and log back in.
4. Verify that you can run docker commands without sudo.
```shell=
docker run hello-world
```

## Docker Swarm

### What is Docker Swarm
Docker Swarm is a cluster-management-and-orchestration tool that makes it easy to scale and manage your already existing Docker services.

### Docker Swarm Component

#### Nodes
A node is an instance of the Docker engine participating in the swarm. You can run one or multiple nodes on a single device, but production deployments typically include Docker nodes distributed across multiple physical devices.

![alt Docker Swarm Nodes](https://miro.medium.com/max/700/0*N6zIu2BIQQpRgPnO.png "Docker Swarm Nodes")

##### Manager Nodes
Manager nodes distribute and schedule incoming tasks onto the worker nodes, maintain the cluster state, and perform orchestration and cluster-management functions. Manager nodes can also optionally run services for worker nodes.

_Note: Docker recommends a maximum of seven manager nodes for a swarm._

##### Worker Nodes
Worker nodes are also instances of the Docker Engine whose sole purpose is to execute containers and services as instructed by the manager nodes.

#### Services
A service is the definition of the tasks to execute on the nodes. It’s the primary root of user interaction with the swarm.

Here’s an example of an HTTP server balancing its load on three replicas:

![alt Docker swarm service with three replicas](https://miro.medium.com/max/700/0*ymlVpJpQi6HpEg1G.png "Docker swarm service with three replicas")

#### Task
A task carries a Docker container and the command that’s executed inside of the container. It’s the atomic scheduling unit of the swarm. Tasks are assigned by the manager node to worker nodes according to the number of replicas set in the service.

### Replicated and Global Services
There are two different ways you can deploy a service, replicated and global.

* Replicated services specify the number of identical tasks (replicas) you want to run. These replicas will then be split up on the different worker nodes and each serves the same content.
* global service is a service that runs one task on every node you have in your swarm and doesn’t need a prespecified number of tasks. Global services are usually used for monitor agents or any other type of container you want to run on every node.

![alt Docker replica visualization](https://miro.medium.com/max/700/0*xkak78Lqe8noKlon.png "Docker replica visualization")

### Getting Started
Now that you know the key concepts of Docker Swarm, we can continue by learning the basics about creating and managing a cluster.

#### Creating Swarm
```shell=
docker swarm init --advertise-addr <MANAGER-IP>
```

#### Add nodes to the swarm
```shell=
docker swarm join --token SWMTKN-1-41r5smr3kgfx780781xxgbenin2dp7qikfh9eketc0wrhrkzsn-8lbew6gpgxwd5fkn52l7s6fof 192.168.65.3:2377
```

The invite token can be displayed using the following command:
```shell=
docker swarm join-token manager
```

#### Viewing the current nodes
```shell=
docker node ls
```

You can promote or demote a node to the manager or worker role. This is useful if a special node is unavailable and you need to replace it.
```shell=
# Promote node to manager 
docker node promote docker-node
 
# Demote node to worker 
docker node demote docker-node
```

#### Leaving the swarm
```shell=
# Leaving the swarm 
docker swarm leave 
#Removing a node from the swarm 
docker node rm worker1
```

#### Deploy a service
```shell=
docker service create --replicas 2 --name webs nginxdemos/hello
```

After that, you can use the service ls command to list all running services:
```shell=
docker service ls 
```

#### Scale a service
```shell=
docker service scale <SERVICE-ID>=<NUMBER-OF-TASKS> 
# For our example 
docker service scale webs=3
```

#### Inspect a service
```shell=
docker service inspect SERVICE_NAME
```

#### Deleting a service
```shell=
docker service rm webs
```
## Ref

### Router
https://github.com/traefik/traefik

### Swarm Grafana
https://grafana.com/grafana/dashboards/609

### Swarm AutoScaling
https://github.com/gianarb/orbiter
https://forums.docker.com/t/autoscaling-in-docker-swarm/44353/2
https://monitor.dockerflow.com/auto-scaling/

```shell=
ssh -i "sofianhw-dev.pem" ubuntu@18.136.102.105
ssh -i "sofianhw-dev.pem" ubuntu@13.213.62.75
```

